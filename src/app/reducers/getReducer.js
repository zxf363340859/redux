

export default (state, action) => {
    switch (action.type) {
        case 'GET_USER_INFO':
            return {
                ...state,
                userInfo: action.userInfo
            };
        default :
            return state;
    }
}
