import React from 'react';
import {Link} from "react-router-dom";
import UserProfile from "../components/UserProfile";
import Provider from "react-redux/es/components/Provider";
import store from '../store'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getUserInfo} from "../action/getAction";

class Home extends React.Component{

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getUserInfo();
    }

    render() {
        return (
            <Provider store={store}>
                <div>
                    <h3>This is a beautiful home page.</h3>
                    <ul>
                        <li>
                            <Link to='/product-details'>Go to Product Details Page</Link>
                        </li>

                    </ul>
                    <h2>User Profiles</h2>
                    <ul>
                        <li>
                            <Link to='/user-profiles/1'>User Profile1</Link>
                        </li>
                        <li>
                            <Link to='/user-profiles/2'>User Profile1</Link>
                        </li>
                    </ul>
                </div>
            </Provider>
        )
    }
};

const mapDispatchToProps = (dispatch) => bindActionCreators({
    getUserInfo
}, dispatch);

const mapStateToProps = (state) => ({
    getReducer: state.getReducer
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
